#
# builder image
#
FROM golang:1.21.3-bookworm as builder
RUN mkdir /build
ADD src/*.go /build/
WORKDIR /build

# accept override of value from --build-args
ARG MY_VERSION=0.0.1
ENV MY_VERSION=$MY_VERSION

# accept override of value from --build-args
ARG MY_BUILDTIME=now
ENV MY_BUILDTIME=$MY_BUILDTIME

# accept override of value from --build-args
ARG MY_GITREF=xxxx
ENV MY_GITREF=$MY_GITREF

# create module, fetch dependencies, then build
RUN go mod init fabianlee.org/docker-golang-hello-world-web \
   && go get -d -u ./... \
   && CGO_ENABLED=0 GOOS=linux go build -ldflags "-X main.Version=${MY_VERSION} -X main.BuildTime=${MY_BUILDTIME} -X main.GitRef=${MY_GITREF}" -a -o fabianlee.org/main .


#
# generate small final image for end users
#
# could have used either alpine or busybox
# busybox-glibc (versus musl) would have better compatability with Debian
FROM busybox:1.36.1-glibc

# copy golang binary into container
WORKDIR /root
COPY --from=builder /build/fabianlee.org/main .

# executable
ENTRYPOINT [ "./main" ]
