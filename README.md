# Continuous Deployment with Agent for Kubernetes and GitLab pipeline with limited privileges

blog: https://fabianlee.org/2023/10/20/gitlab-continuous-deployment-with-agent-for-kubernetes-and-gitlab-pipeline/

Shows how to deploy the GitLab Agent for Kubernetes into a GitLab project with limited privileges for the Agent service account.

This will grant the pipeline access to only a select set of resources when using kubectl across the Kubernetes cluster.

# GitLab Pipeline

The [GitLab pipeline](.gitlab-ci.yaml) performs two major tasks:

  * Builds/Publishes image for simple GoLang webserver, deploys to Cluster
  * Deploys bitnami/nginx Helm Chart into Cluster


# Pulling image from GitHub Container Registry

```
docker pull registry.gitlab.com/gitlab-agent2/gitlab-agent-for-k8s-helm:latest
```

# Creating tag that invokes Github Action

```
newtag=v1.0.1
git commit -a -m "changes for new tag $newtag" && git push -o ci.skip
git tag $newtag && git push origin $newtag
```

# Deleting tag

```
# delete local tag, then remote
todel=v1.0.1
git tag -d $todel && git push -d origin $todel
```

